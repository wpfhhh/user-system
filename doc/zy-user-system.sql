/*
SQLyog Ultimate v12.08 (64 bit)
MySQL - 5.7.26 : Database - zy-user-system
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `resource` */

CREATE TABLE `resource` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(50) NOT NULL COMMENT '名字',
  `url` varchar(50) NOT NULL COMMENT '地址',
  `pid` int(11) DEFAULT NULL COMMENT '父级id',
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `order_number` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4;

/*Data for the table `resource` */

insert  into `resource`(`id`,`name`,`url`,`pid`,`create_time`,`update_time`,`order_number`) values (1,'用户管理','/user',0,'2020-07-07 09:50:33','2020-07-07 09:50:36',20);
insert  into `resource`(`id`,`name`,`url`,`pid`,`create_time`,`update_time`,`order_number`) values (2,'用户添加','/userEdit',1,'2020-07-07 14:38:18','2020-07-07 14:38:41',NULL);
insert  into `resource`(`id`,`name`,`url`,`pid`,`create_time`,`update_time`,`order_number`) values (3,'用户修改','/userEdit',1,'2020-07-07 14:38:20','2020-07-07 14:38:43',NULL);
insert  into `resource`(`id`,`name`,`url`,`pid`,`create_time`,`update_time`,`order_number`) values (4,'用户删除','/user',1,'2020-07-07 09:50:38','2020-07-07 14:38:45',NULL);
insert  into `resource`(`id`,`name`,`url`,`pid`,`create_time`,`update_time`,`order_number`) values (5,'用户查询','/user',1,'2020-07-07 14:38:25','2020-07-07 14:38:47',NULL);
insert  into `resource`(`id`,`name`,`url`,`pid`,`create_time`,`update_time`,`order_number`) values (6,'角色管理','/role',0,'2020-07-07 14:38:27','2020-07-07 14:40:26',30);
insert  into `resource`(`id`,`name`,`url`,`pid`,`create_time`,`update_time`,`order_number`) values (7,'角色添加','/roleEdit',6,'2020-07-07 09:50:39','2020-07-07 14:38:48',NULL);
insert  into `resource`(`id`,`name`,`url`,`pid`,`create_time`,`update_time`,`order_number`) values (8,'角色修改','/roleEdit',6,'2020-07-07 14:38:39','2020-07-07 14:38:50',NULL);
insert  into `resource`(`id`,`name`,`url`,`pid`,`create_time`,`update_time`,`order_number`) values (12,'角色查询','/role',6,'2020-07-07 16:23:28','2020-07-09 10:17:27',NULL);
insert  into `resource`(`id`,`name`,`url`,`pid`,`create_time`,`update_time`,`order_number`) values (13,'角色删除','/roleDelete',6,'2020-07-07 16:23:46','2020-07-07 16:27:44',NULL);
insert  into `resource`(`id`,`name`,`url`,`pid`,`create_time`,`update_time`,`order_number`) values (14,'角色授权','/editRoleOfResource',6,'2020-07-07 16:24:11','2020-07-09 10:17:29',NULL);
insert  into `resource`(`id`,`name`,`url`,`pid`,`create_time`,`update_time`,`order_number`) values (15,'资源管理','/resource',0,'2020-07-07 16:24:41','2020-07-09 10:17:31',40);
insert  into `resource`(`id`,`name`,`url`,`pid`,`create_time`,`update_time`,`order_number`) values (16,'资源添加','/resourceEdit',15,'2020-07-07 16:25:15','2020-07-12 17:33:40',4);
insert  into `resource`(`id`,`name`,`url`,`pid`,`create_time`,`update_time`,`order_number`) values (17,'资源删除','/resource',15,'2020-07-07 16:25:37','2020-07-12 17:33:34',3);
insert  into `resource`(`id`,`name`,`url`,`pid`,`create_time`,`update_time`,`order_number`) values (19,'资源修改','/resourceEdit',15,'2020-07-07 16:26:09','2020-07-12 17:33:27',2);
insert  into `resource`(`id`,`name`,`url`,`pid`,`create_time`,`update_time`,`order_number`) values (20,'资源查询','/resource',15,'2020-07-07 16:27:05','2020-07-12 17:32:59',1);
insert  into `resource`(`id`,`name`,`url`,`pid`,`create_time`,`update_time`,`order_number`) values (21,'首页','/index',0,'2020-07-07 16:32:35','2020-07-09 10:17:39',10);
insert  into `resource`(`id`,`name`,`url`,`pid`,`create_time`,`update_time`,`order_number`) values (22,'资源授权','/authorize',6,'2020-07-13 15:45:22',NULL,1);

/*Table structure for table `role` */

CREATE TABLE `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `remark` text,
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;

/*Data for the table `role` */

insert  into `role`(`id`,`name`,`remark`,`create_time`,`update_time`) values (1,'超级管理员','拥有整个网站的全部权限','2020-07-06 15:13:38','2020-07-06 15:03:08');
insert  into `role`(`id`,`name`,`remark`,`create_time`,`update_time`) values (2,'用户管理员','用户基本信息修改[用户添加，用户修改，用户查询，<br>用户修改密码，用户删除]','2020-07-09 10:17:07','2020-07-09 10:16:59');
insert  into `role`(`id`,`name`,`remark`,`create_time`,`update_time`) values (5,'普通用户','普通用户，只能查看网站首页','2020-07-07 16:30:53','2020-07-07 16:31:13');
insert  into `role`(`id`,`name`,`remark`,`create_time`,`update_time`) values (6,'测试','测试','2020-07-10 10:56:32','2020-07-12 14:49:33');

/*Table structure for table `role_resource` */

CREATE TABLE `role_resource` (
  `role_id` int(11) NOT NULL,
  `resource_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `role_resource` */

insert  into `role_resource`(`role_id`,`resource_id`) values (1,2);
insert  into `role_resource`(`role_id`,`resource_id`) values (1,3);
insert  into `role_resource`(`role_id`,`resource_id`) values (1,4);
insert  into `role_resource`(`role_id`,`resource_id`) values (1,5);
insert  into `role_resource`(`role_id`,`resource_id`) values (1,6);
insert  into `role_resource`(`role_id`,`resource_id`) values (1,7);
insert  into `role_resource`(`role_id`,`resource_id`) values (1,8);
insert  into `role_resource`(`role_id`,`resource_id`) values (1,12);
insert  into `role_resource`(`role_id`,`resource_id`) values (1,13);
insert  into `role_resource`(`role_id`,`resource_id`) values (1,15);
insert  into `role_resource`(`role_id`,`resource_id`) values (1,16);
insert  into `role_resource`(`role_id`,`resource_id`) values (1,17);
insert  into `role_resource`(`role_id`,`resource_id`) values (1,19);
insert  into `role_resource`(`role_id`,`resource_id`) values (1,20);
insert  into `role_resource`(`role_id`,`resource_id`) values (1,21);
insert  into `role_resource`(`role_id`,`resource_id`) values (1,1);
insert  into `role_resource`(`role_id`,`resource_id`) values (2,1);
insert  into `role_resource`(`role_id`,`resource_id`) values (2,2);
insert  into `role_resource`(`role_id`,`resource_id`) values (2,3);
insert  into `role_resource`(`role_id`,`resource_id`) values (2,4);
insert  into `role_resource`(`role_id`,`resource_id`) values (2,5);
insert  into `role_resource`(`role_id`,`resource_id`) values (2,21);
insert  into `role_resource`(`role_id`,`resource_id`) values (5,21);
insert  into `role_resource`(`role_id`,`resource_id`) values (1,22);
insert  into `role_resource`(`role_id`,`resource_id`) values (1,14);

/*Table structure for table `user` */

CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `login_name` varchar(50) NOT NULL,
  `login_password` varchar(100) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `role_pk` (`role_id`),
  CONSTRAINT `role_pk` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=92 DEFAULT CHARSET=utf8mb4;

/*Data for the table `user` */

insert  into `user`(`id`,`name`,`login_name`,`login_password`,`role_id`,`create_time`,`update_time`) values (87,'管理员','admin','123',1,'2020-07-09 10:17:48','2020-07-09 16:58:45');
insert  into `user`(`id`,`name`,`login_name`,`login_password`,`role_id`,`create_time`,`update_time`) values (88,'普通用户','user','123',5,'2020-07-07 16:31:30','2020-07-09 10:17:52');
insert  into `user`(`id`,`name`,`login_name`,`login_password`,`role_id`,`create_time`,`update_time`) values (91,'管理员','wpf','123',2,'2020-07-12 17:32:16','2020-07-13 10:10:38');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

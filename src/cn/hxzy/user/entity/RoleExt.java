package cn.hxzy.user.entity;

import java.util.List;

public class RoleExt {
    private List<Resource> resources;

    public List<Resource> getResources() {
        return resources;
    }

    public void setResources(List<Resource> resources) {
        this.resources = resources;
    }
}

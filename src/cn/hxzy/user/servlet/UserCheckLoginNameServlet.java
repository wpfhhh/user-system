package cn.hxzy.user.servlet;

import cn.hxzy.user.service.UserService;
import com.alibaba.fastjson.JSON;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

@WebServlet("/userCheckLoginName")
public class UserCheckLoginNameServlet extends HttpServlet {
    UserService userService = new UserService();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            String name = req.getParameter("name");
            boolean b = userService.findByLoginName(name);
            PrintWriter writer = resp.getWriter();
            Map map = new HashMap();

            if (b) {
                map.put("code", 300);
                map.put("msg", "账号已存在!");
            } else {
                map.put("code", 200);
                map.put("msg", "账号可以使用!");
            }
            writer.write(JSON.toJSONString(map));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

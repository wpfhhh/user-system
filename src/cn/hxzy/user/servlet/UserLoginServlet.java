package cn.hxzy.user.servlet;

import cn.hxzy.user.entity.Role;
import cn.hxzy.user.entity.User;
import cn.hxzy.user.service.RoleService;
import cn.hxzy.user.service.UserService;
import cn.hxzy.user.util.VerifyUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/login")
public class UserLoginServlet extends HttpServlet {
    private UserService userService = new UserService();
    private RoleService roleService = new RoleService();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("WEB-INF/jsp/login.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            String verify = req.getParameter("verify");
            req.setAttribute("verify", verify);
            req.setAttribute("message", "验证码错误");
            if (verify != null && verify != "") {
                req.setAttribute("message", "用户名密码错误");
                Object attribute = req.getSession().getAttribute(VerifyUtil.RANDOMCODEKEY);
                if (verify.equals(attribute)) {
                    String loginName = req.getParameter("loginName");
                    req.setAttribute("loginName", loginName);
                    String loginPassword = req.getParameter("loginPassword");
                    req.setAttribute("loginPassword", loginPassword);
                    User user = userService.login(loginName, loginPassword);
                    if (user != null) {
                        req.getSession().setAttribute("loginUser", user);
                        Role role = roleService.findByIdWithResource(user.getRoleId());
                        req.getSession().setAttribute("loginRole", role);
                        resp.sendRedirect("index");
                        return;
                    }
                }
            }
            req.getRequestDispatcher("WEB-INF/jsp/login.jsp").forward(req, resp);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">

<nav class="navbar navbar-inverse" style="border-radius: 0">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                    aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">智原用户管理系统</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">

            <ul class="nav navbar-nav">
                <c:forEach items="${loginRole.resources}" var="r">
                    <c:if test="${ r.pid == 0}">
                        <li class="${pageContext.request.requestURI.endsWith(r.url.concat(".jsp"))?'active':''}">
                            <a href="${pageContext.request.contextPath}${r.url}">${r.name}</a>
                        </li>
                    </c:if>
                </c:forEach>

            </ul>
            <ul class="nav navbar-nav navbar-right hidden-sm">
                <li><a>${loginUser.name}</a></li>
                <li><a href="logout">退出</a></li>
            </ul>
        </div>

    </div>
</nav>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>登录</title>
    <style>
        body{
            background: url("${pageContext.request.contextPath}/img/timg.jpg");
        }
        .form-signin {
            max-width: 500px;
            padding: 15px;
            margin: 100px auto;
            background:rgba(233,233,233,0.3);
            border-radius: 6px;
            color: white;
            text-align: center;
            color:#222;
        }
        .form-signin img{
            float: left;
        }
    </style>
    <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<div class="container">
    <form class="form-horizontal form-signin" action="login" method="post">
        <h2 class="form-signin-heading">用户管理系统</h2>
        <br>
        <br>
        <div class="form-group">
            <label for="inputEmail3" class="col-xs-2 control-label">账号</label>
            <div class="col-xs-10">
                <input type="text" class="form-control" id="inputEmail3" name="loginName" placeholder="登录名" value="${loginName}" required>
            </div>
        </div>
        <div class="form-group">
            <label for="inputPassword3" class="col-xs-2 control-label">密码</label>
            <div class="col-xs-10">
                <input type="password" class="form-control" id="inputPassword3" name="loginPassword" placeholder="密码" value="${loginPassword}" required>
            </div>
        </div>

        <div class="form-group">
            <label for="inputPassword4" class="col-xs-2 control-label">验证码</label>
            <label for="inputPassword4" class="col-xs-4"><img  src="verify" onclick="this.src='verify?'+Math.random()"></label>
            <div class="col-xs-6">
                <input type="text" class="form-control" id="inputPassword4" name="verify"  placeholder="验证码" required value="${verify}">
            </div>
        </div>
        <br>
        <div class="form-group">
            <div class="col-xs-12">
                <p style="color: red">${message}</p>
                <button type="submit" class="btn btn-success btn-block btn-lg">登录</button>
            </div>
        </div>
    </form>

<%@ include file="inc/footer.jsp"%>
</div>
</body>
</html>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>资源管理</title>
</head>
<body>
<%@ include file="inc/header.jsp" %>
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <h1>资源管理</h1>
            <a href="resourceEdit" type="button" class="btn btn-success">添加</a>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12" style="margin-top: 20px">

            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                <c:forEach items="${resourceList}" var="r">
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="heading${r.id}">
                            <h4 class="panel-title" style="line-height: 2">
                                <a role="button" class="collapsed" data-toggle="collapse" data-parent="#accordion"
                                   href="#${r.name}"
                                   aria-expanded="false" aria-controls="${r.name}">
                                        ${r.name} ${r.url} ${r.orderNumber}
                                    <p class="pull-right">
                                        <a href="resourceEdit?id=${r.id}"
                                           class="btn btn-warning btn-sm">修改
                                        </a>
                                        <a href="resource?id=${r.id}"
                                           class="btn btn-danger btn-sm">
                                            删除
                                        </a>
                                    </p>

                                </a>
                            </h4>
                        </div>
                        <div id="${r.name}" class="panel-collapse collapse" role="tabpanel"
                             aria-labelledby="heading${r.id}">
                            <ul class="panel-body">
                                <c:forEach items="${r.son}" var="s">
                                    <li class="list-group-item" style="line-height: 2">
                                            ${s.name} ${s.url} ${s.orderNumber}
                                        <p class="pull-right">
                                            <a href="resourceEdit?id=${s.id}"
                                               class="btn btn-warning btn-sm">
                                                修改
                                            </a>
                                            <a href="resource?id=${s.id}"
                                               class="btn btn-danger btn-sm">
                                                删除
                                            </a>
                                        </p>
                                    </li>
                                </c:forEach>
                            </ul>

                        </div>
                    </div>
                </c:forEach>

            </div>


        </div>


    </div>


</div>


<%@ include file="inc/footer.jsp" %>

</body>
</html>

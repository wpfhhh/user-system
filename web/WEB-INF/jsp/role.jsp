<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>角色管理</title>
</head>
<body>
<%@ include file="inc/header.jsp" %>
<div class="container">
    <div class="row">
        <h1>角色管理</h1>
        <a href="roleEdit" type="button" class="btn btn-success">添加</a>
    </div>

    <div class="row">
        <table class="table table-hover">
            <thead>
            <tr>
                <th>ID</th>
                <th>名字</th>
                <th>备注</th>
                <th>创建时间</th>
                <th>修改时间</th>
                <th>编辑</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${roleList}" var="u">
                <tr>
                    <td>${u.id}</td>
                    <td>${u.name}</td>
                    <td>${u.remark}</td>
                    <td>${u.createTime}</td>
                    <td>${u.updateTime}</td>
                    <td>
                        <a href="editRoleOfResource?id=${u.id}" type="button" class="btn btn-warning btn-sm">
                            授权
                        </a>
                        <a href="roleEdit?id=${u.id}" type="button" class="btn btn-info btn-sm">
                            修改
                        </a>
                        <button onclick="deleteRole(${u.id})" type="button" class="btn btn-danger btn-sm">
                            删除
                        </button>
                    </td>
                </tr>
            </c:forEach>

            </tbody>
        </table>

    </div>


</div>


<%@ include file="inc/footer.jsp" %>
<script>
    function deleteRole(id) {
        $.post("roleDelete", {id: id}, function (response) {
            var obj = JSON.parse(response)
            if (obj.code == 200) {
              location.reload();
            }else {
                alert(obj.msg);
            }
        })
    }
</script>
</body>
</html>

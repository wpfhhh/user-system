<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>角色编辑</title>
</head>
<body>
<%@ include file="inc/header.jsp" %>

<div class="container">
    <form class="form-horizontal" action="" method="post">
        <input name="id" value="${role.id}" hidden>

        <div class="form-group">
            <label for="name" class="col-sm-2 control-label">角色名</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="name" placeholder="角色名" name="name"
                       value="${role.name}">
            </div>
        </div>

        <div class="form-group">
            <label for="remark" class="col-sm-2 control-label">备注</label>
            <div class="col-sm-10">
                <textarea name="remark" class="form-control" id="remark">${role.remark}</textarea>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-default">保存</button>
            </div>
        </div>
    </form>

</div>


<%@ include file="inc/footer.jsp" %>

</body>
</html>

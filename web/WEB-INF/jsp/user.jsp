<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>用户管理</title>
</head>
<body>
<%@ include file="inc/header.jsp" %>
<div class="container">
    <div class="row">
        <h1>用户管理</h1>
        <a href="userEdit" type="button" class="btn btn-success">
            添加
        </a>
        <div class="input-group" style="float: right;width: 30%">
            <input type="text" class="form-control" placeholder="姓名" id="search" value="${name}">
            <div class="input-group-btn">
                <button class="btn btn-default" type="button" onclick="searchName()">搜索</button>
            </div>
        </div>
    </div>
    <div class="row">
        <table class="table table-hover">
            <thead>
            <tr>
                <th>ID</th>
                <th>姓名</th>
                <th>登录名</th>
                <th>密码</th>
                <th>角色</th>
                <th>创建时间</th>
                <th>修改时间</th>
                <th> 编辑</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${userPage.list}" var="u">
                <tr>
                    <td>${u.id}</td>
                    <td>${u.name}</td>
                    <td>${u.loginName}</td>
                    <td>${u.loginPassword}</td>
                    <td>${u.role.name}</td>
                    <td>${u.createTime}</td>
                    <td>${u.updateTime}</td>
                    <td>
                        <a href="userEdit?id=${u.id}" type="button" class="btn btn-info btn-sm">
                            修改
                        </a>
                        <a href="user?id=${u.id}" type="button" class="btn btn-danger btn-sm">
                            删除
                        </a>
                    </td>
                </tr>
            </c:forEach>

            </tbody>
        </table>
        <ul class="pagination">
            <li><a href="user?page=${userPage.page-1<1?1:(userPage.page-1)}&name=${name}" aria-label="Previous">上一页</a>
            </li>
            <c:forEach begin="1" end="${userPage.pageCount}" step="1" var="index">
                <li class="${userPage.page==index?'active':''}"><a href="user?page=${index}&name=${name}">${index}</a>
                </li>
            </c:forEach>
            <li>
                <a href="user?page=${(userPage.page+1>userPage.pageCount)?userPage.pageCount:(userPage.page+1)}&name=${name}">下一页 </a>
            </li>
        </ul>
    </div>
</div>


<%@ include file="inc/footer.jsp" %>
<script>
    function searchName() {
        location.href = "user?name=" + $("#search").val()

    }
</script>
</body>
</html>

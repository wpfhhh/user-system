<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>用户添加</title>
</head>
<body>
<%@ include file="inc/header.jsp" %>

<div class="container">

    <form class="form-horizontal" action="" method="post">
        <input name="id" value="${user.id}" hidden>
        <div class="form-group">
            <label for="name" class="col-sm-2 control-label">姓名</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="name" placeholder="姓名" name="name" value="${user.name}">
            </div>
        </div>
        <div class="form-group">
            <label for="loginName" class="col-sm-2 control-label">登录名<span id="nameCheckInfo" style="color: red"></span></label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="loginName" placeholder="登录名" name="loginName"
                       value="${user.loginName}" onblur="checkLoginName()">
            </div>
        </div>

        <div class="form-group">
            <label for="loginPassword" class="col-sm-2 control-label">密码</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="loginPassword" placeholder="密码" name="loginPassword"
                       value="${user.loginPassword}">
            </div>
        </div>
        <div class="form-group">
            <label for="role" class="col-sm-2 control-label">角色</label>
            <div class="col-sm-10">
                <select id="role" name="roleId" class="form-control">
                    <c:forEach items="${roleList}" var="r">
                        <option ${user.role.id==r.id?'selected':''} value="${r.id}">${r.name}</option>
                    </c:forEach>
                </select>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-default">保存</button>
            </div>
        </div>
    </form>
</div>


<%@ include file="inc/footer.jsp" %>
<script>
    function checkLoginName() {
        $.post("userCheckLoginName", {name: $("#loginName").val()}, function (response) {
            var obj = JSON.parse(response)
            if (obj.code != 200) {
                alert(obj.msg);
                $("#loginName").val('');
            }
        })
    }
</script>
</body>
</html>
